const options = {
  /** 
   * By default the final result is always predictable, you can add an extra layer
   * of randomness by adding special characters or words
   * @type {string[]}
   */
  salt: [],
  /** Separator for each group */
  separator: '-',
}

Object.freeze(options)

/**
 * Creates a 16~17 length number based on a single number
 * @param {number} seed 
 */
const pseudoRandom = function (seed) {
  if (isNaN(seed)) {
    return 0
  }
  const firstStep = Math.sin(seed) * 10000;
  // Reduces the number into between 0 and 1 (preserving the defined random expected output)
  const secondStep = firstStep - Math.floor(firstStep)
  return secondStep
}

/**
 * Creates a pseudo-predictable hash from a combination of words
 * @param {string[]} words String factors for creation of hash
 */
const makeHash = function (words, opts = options) {
  if (Array.isArray(words) === false) {
    throw new Error(`Expected first argument to be an Array but instead got ${typeof words}`)
  }

  const _options = Object.assign({}, options, opts);
  const {
    salt,
    separator
  } = _options;
  const wordsWithSalt = []

  // Option validation
  if (Array.isArray(salt) === false) {
    throw new Error(`Expected salt option to be an Array but instead got ${typeof salt}`)
  }

  for (let index = 0; index < words.length; index++) {
    const saltWord = salt[Math.floor(pseudoRandom(index) * salt.length)] || ""
    wordsWithSalt.push(saltWord, words[index])
  }
  // Creates a buffer for each string and sums each bit value into a single number
  const buffers = wordsWithSalt.map((word) => {
    const buffer = Buffer.from(word)
    let sum = 0;
    let count = 0;
    for (const bit of buffer.values()) {
      // Will change operation depending if odd or even
      if (count % 2) {
        sum += bit
      } else {
        sum -= bit
      }
      count++;
    }
    return Math.abs(sum);
  })
  // Concats all the values into a single number
  const seed = Number(buffers.reduce((previousValue, currentValue) => `${previousValue}${currentValue}`))
  const rando = pseudoRandom(seed).toString()
  // Removes the decimal indicator from the number
  const protoHash = Math.abs(
    rando.replace(/\./, '')
  ).toString().substring(0, 16).match(/.{1,4}/g) // Gets the first 16 characters and separates the number into a group of 4 characters
  // Adds the separator
  const hash = protoHash.join(separator)

  return hash
}

module.exports = makeHash