<p align="center">
  <img src="https://i.imgur.com/BL0lf5F.png" width="240" height="240" alt="prerender-loader">
  <h1 align="center">
    predictable-hash
    <a href="https://www.npmjs.com/package/predictable-hash"><img src="https://img.shields.io/npm/v/predictable-hash.svg?lstyle=flat-square" alt="npm"></a>
  </h1>
</p>

Small lib that makes predictable hashes out of an array of words

# Usage

Without options
```js
const predictableHash = require("predictable-hash")
const hash = predictableHash(["my", "combination"])
// returns 8444-4245-5781-0635
```

Salting the result
```js
const predictableHash = require("predictable-hash")
const hash = predictableHash(["my", "combination"], { salt: ["S", "A", "L", "T", "Y"] })
// returns 2847-6324-1670-5892
```

# Options
| Option      | Type     | Default | Description                                                                 |
|-------------|----------|---------|-----------------------------------------------------------------------------|
| `salt`      | string[] | []      | Characters that are pseudo-randomly added between the provided word array          |
| `separator` | string   | -       | Separator for each group of numbers.  If undefined, it will use `,` instead |