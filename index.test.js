const predictableHash = require("./index")

test("without salt", () => {
  const hash = predictableHash(["my", "combination"])
  expect(hash).toBe("8444-4245-5781-0635")
})

test("with custom separator", () => {
  const hash = predictableHash(["my", "combination"], {
    separator: " - "
  })
  expect(hash).toBe("8444 - 4245 - 5781 - 0635")
})

test("with salt", () => {
  const hash = predictableHash(["my", "combination"], {
    salt: ["S", "A", "L", "T", "Y"]
  })
  expect(hash).toBe("2847-6324-1670-5892")
})

test("with salt and custom separator", () => {
  const hash = predictableHash(["my", "combination"], {
    salt: ["S", "A", "L", "T", "Y"],
    separator: " - "
  })
  expect(hash).toBe("2847 - 6324 - 1670 - 5892")
})

test("without words should fail", () => {
  expect(() => {
    predictableHash(undefined)
  }).toThrowError()
})

test("with salt as undefined should fail", () => {
  expect(() => {
    predictableHash(["my", "combination"], {
      salt: undefined
    })
  }).toThrowError()
})

test("with separator as undefined", () => {
  const hash = predictableHash(["my", "combination"], {
    separator: undefined
  })

  expect(hash).toBe("8444,4245,5781,0635")
})